package HRM;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import junit.framework.Assert;

public class Activity1 {
	WebDriver driver;
	
  @Test
  public void test() {
	  System.out.println("Title is " + driver.getTitle());
	  Assert.assertEquals(driver.getTitle(), "OrangeHRM");
  }
  @BeforeClass
  public void beforeClass() {
	  driver = new FirefoxDriver();
	  driver.get("http://alchemy.hguy.co/orangehrm");
  }

  @AfterClass
  public void afterClass() {
	  driver.close();
  }

}
