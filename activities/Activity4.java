package HRM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import junit.framework.Assert;

public class Activity4 {
	WebDriver driver;
	WebDriverWait wait;

	@Test
	public void test() {
		wait = new WebDriverWait(driver, 10);
		driver.findElement(By.xpath("//*[@id=\"txtUsername\"]")).sendKeys("orange");
		driver.findElement(By.xpath("//*[@id=\"txtPassword\"]")).sendKeys("orangepassword123");
		driver.findElement(By.xpath("//*[@id=\"btnLogin\"]")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[1]/div[2]/ul/li[2]/a/b")));
		driver.findElement(By.xpath("/html/body/div[1]/div[2]/ul/li[2]/a/b")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"menu_pim_addEmployee\"]")));
		driver.findElement(By.xpath("//*[@id=\"menu_pim_addEmployee\"]")).click();
		driver.findElement(By.xpath("//*[@id=\"firstName\"]")).sendKeys("Test");
		driver.findElement(By.xpath("//*[@id=\"lastName\"]")).sendKeys("Tester");
		driver.findElement(By.xpath("//*[@id=\"chkLogin\"]")).click();
		driver.findElement(By.xpath("//*[@id=\"user_name\"]")).sendKeys("AATesting22");
		driver.findElement(By.xpath("//*[@id=\"btnSave\"]")).click();
		driver.findElement(By.xpath("//*[@id=\"menu_admin_viewAdminModule\"]")).click();
		WebElement verify = driver.findElement(By.linkText("AATesting22"));
		Assert.assertEquals(verify.getText(), "AATesting22");

	}

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.get("http://alchemy.hguy.co/orangehrm");
	}

	@AfterClass
	public void afterClass() {
		driver.close();
	}

}
