package HRM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity5 {

	WebDriver driver;

	@Test 
	public void test() {
		driver.findElement(By.xpath("//*[@id=\"txtUsername\"]")).sendKeys("orange");
		driver.findElement(By.xpath("//*[@id=\"txtPassword\"]")).sendKeys("orangepassword123");
		driver.findElement(By.xpath("//*[@id=\"btnLogin\"]")).click();
		driver.findElement(By.xpath("/html/body/div[1]/div[2]/ul/li[6]/a/b")).click();
		driver.findElement(By.xpath("//*[@id=\"btnSave\"]")).click();
		driver.findElement(By.xpath("//*[@id=\"personal_txtEmpFirstName\"]")).clear();
		driver.findElement(By.xpath("//*[@id=\"personal_txtEmpFirstName\"]")).sendKeys("Test");
		driver.findElement(By.xpath("//*[@id=\"personal_txtEmpLastName\"]")).clear();
		driver.findElement(By.xpath("//*[@id=\"personal_txtEmpLastName\"]")).sendKeys("Tester");
		driver.findElement(By.xpath("//*[@id=\"personal_optGender_1\"]")).click();
		Select Dropdown = new Select(driver.findElement(By.xpath("//*[@id=\"personal_cmbNation\"]")));
		Dropdown.selectByValue("82");
		driver.findElement(By.xpath("//*[@id=\"personal_DOB\"]")).clear();;
		driver.findElement(By.xpath("//*[@id=\"personal_DOB\"]")).sendKeys("1992-01-01");
		driver.findElement(By.xpath("//*[@id=\"btnSave\"]")).click();

	}

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		driver.get("http://alchemy.hguy.co/orangehrm");
	}

	@AfterClass
	public void afterClass() {
		driver.close();
	}

}
