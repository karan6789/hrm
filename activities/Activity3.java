package HRM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import junit.framework.Assert;

public class Activity3 {
	WebDriver driver;
	WebDriverWait wait;
	
  @Test
  public void test() {
	  driver.findElement(By.xpath("//*[@id=\"txtUsername\"]")).sendKeys("orange");
	  driver.findElement(By.xpath("//*[@id=\"txtPassword\"]")).sendKeys("orangepassword123");
	  driver.findElement(By.xpath("//*[@id=\"btnLogin\"]")).click();
	  wait = new WebDriverWait(driver , 10);
	  WebElement user = driver.findElement(By.xpath("//*[@id=\"welcome\"]"));
	  Assert.assertEquals(user.getText(), "Welcome Test1");
	  
  }
  @BeforeClass
  public void beforeClass() {
	  driver = new FirefoxDriver();
	  driver.get("http://alchemy.hguy.co/orangehrm");
  }

  @AfterClass
  public void afterClass() {
	  driver.close();
  }

}
