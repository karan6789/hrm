package HRM;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity9 {
	WebDriver driver;

	@Test(priority = 0)
	public void login() {
		driver.findElement(By.xpath("//*[@id=\"txtUsername\"]")).sendKeys("orange");
		driver.findElement(By.xpath("//*[@id=\"txtPassword\"]")).sendKeys("orangepassword123");
		driver.findElement(By.xpath("//*[@id=\"btnLogin\"]")).click();
		driver.findElement(By.xpath("//*[@id=\"menu_dashboard_index\"]")).click();
	}

	@Test(priority = 1)
	public void contacts() {
		driver.findElement(By.xpath("//*[@id='menu_pim_viewMyDetails']")).click();
		WebElement leftmenu = driver.findElement(By.xpath("//ul[@id = 'sidenav']"));
		WebElement contact = leftmenu.findElement(By.linkText("Emergency Contacts"));
		contact.click();
		// locate table
		WebElement table = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div/div[3]/div[2]/form/table/tbody"));
		// locate rows
		List <WebElement> rows = table.findElements(By.tagName("tr"));
		//loop
		for(int row=0; row < rows.size(); row++  ) {
		System.out.println("Details are : " + rows.get(row).getText() );
		}
		
		
	}

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.get("http://alchemy.hguy.co/orangehrm");
	}

	@AfterClass
	public void afterClass() {
		driver.close();
	}

}
