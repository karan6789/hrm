package HRM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import junit.framework.Assert;

public class Activity11 {
	WebDriver driver;

	@Test(priority = 0)
	public void login() {
		driver.findElement(By.xpath("//*[@id=\"txtUsername\"]")).sendKeys("orange");
		driver.findElement(By.xpath("//*[@id=\"txtPassword\"]")).sendKeys("orangepassword123");
		driver.findElement(By.xpath("//*[@id=\"btnLogin\"]")).click();
		driver.findElement(By.xpath("//*[@id=\"menu_dashboard_index\"]")).click();
	}

	@Test(priority = 1)
	public void recruitment() {
		driver.findElement(By.xpath("//a[@id='menu_recruitment_viewRecruitmentModule']")).click();
		driver.findElement(By.xpath("//input[@id='btnAdd']")).click();

		driver.findElement(By.xpath("//input[@id = 'addCandidate_firstName']")).sendKeys("Test55");
		driver.findElement(By.xpath("//input[@id = 'addCandidate_lastName']")).sendKeys("Tester55");
		driver.findElement(By.xpath("//input[@id = 'addCandidate_email']")).sendKeys("Test@tester.com");
		WebElement resume = driver.findElement(By.xpath("//input[@id = 'addCandidate_resume']"));
		resume.sendKeys("C:\\Users\\KaranArora\\Downloads\\Doc1.docx");
		driver.findElement(By.xpath("//input[@id='btnSave']")).click();
		driver.findElement(By.xpath("//input[@id='btnBack']")).click();
		
		driver.findElement(By.xpath("//input[@id = 'candidateSearch_candidateName']")).sendKeys("Test55 Tester55");
		driver.findElement(By.xpath("//input[@id = 'btnSrch']")).click();
		
		WebElement verify = driver.findElement(By.linkText("Test55 Tester55"));
		Assert.assertEquals(verify.getText(), "Test55 Tester55");
	}

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.get("http://alchemy.hguy.co/orangehrm");
	}

	@AfterClass
	public void afterClass() {
		driver.close();
	}

}
