package HRM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import junit.framework.Assert;

public class Activity10 {
	WebDriver driver;

	@Test(priority = 0)
	public void login() {
		driver.findElement(By.xpath("//*[@id=\"txtUsername\"]")).sendKeys("orange");
		driver.findElement(By.xpath("//*[@id=\"txtPassword\"]")).sendKeys("orangepassword123");
		driver.findElement(By.xpath("//*[@id=\"btnLogin\"]")).click();
		driver.findElement(By.xpath("//*[@id=\"menu_dashboard_index\"]")).click();
	}

	@Test(priority = 1)
	public void contacts() {
		driver.findElement(By.xpath("//a[@id='menu_recruitment_viewRecruitmentModule']")).click();
		driver.findElement(By.xpath("//a[@id='menu_recruitment_viewJobVacancy']")).click();
		driver.findElement(By.xpath("//input[@id='btnAdd']")).click();
		Select title = new Select(driver.findElement(By.xpath("//select[@id = 'addJobVacancy_jobTitle']")));
		title.selectByVisibleText("DevOps Engineer");
		driver.findElement(By.xpath("//input[@id = 'addJobVacancy_name']")).sendKeys("Vacancy Test1");
		driver.findElement(By.xpath("//input[@id = 'addJobVacancy_hiringManager']")).sendKeys("Test Tester");
		driver.findElement(By.xpath("//input[@id='btnSave']")).click();
		driver.findElement(By.xpath("//input[@id='btnBack']")).click();

		Select title1 = new Select(driver.findElement(By.xpath("//select[@id = 'vacancySearch_jobTitle']")));
		title1.selectByVisibleText("DevOps Engineer");

		Select manager = new Select(driver.findElement(By.xpath("//select[@id = 'vacancySearch_hiringManager']")));
		manager.selectByVisibleText("Test Tester");

		driver.findElement(By.xpath("//input[@id='btnSrch']")).click();

		WebElement verify = driver.findElement(By.linkText("Vacancy Test1"));
		Assert.assertEquals(verify.getText(), "Vacancy Test1");
	}

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.get("http://alchemy.hguy.co/orangehrm");
	}

	@AfterClass
	public void afterClass() {
		driver.close();
	}

}
