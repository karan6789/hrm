package HRM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import junit.framework.Assert;

public class Activity8 {
	WebDriver driver;

	@Test(priority = 0)
	public void login() {
		driver.findElement(By.xpath("//*[@id=\"txtUsername\"]")).sendKeys("orange");
		driver.findElement(By.xpath("//*[@id=\"txtPassword\"]")).sendKeys("orangepassword123");
		driver.findElement(By.xpath("//*[@id=\"btnLogin\"]")).click();
		driver.findElement(By.xpath("//*[@id=\"menu_dashboard_index\"]")).click();
	}

	@Test(priority = 1)
	public void leave() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		driver.findElement(By.xpath("//span[contains(text(), 'Apply Leave')]")).click();
		Select leavetype = new Select(driver.findElement(By.xpath("//select[@id = 'applyleave_txtLeaveType']")));
		leavetype.selectByVisibleText("Paid Leave");
		driver.findElement(By.xpath("//*[@id=\"applyleave_txtFromDate\"]")).clear();
		driver.findElement(By.xpath("//*[@id=\"applyleave_txtFromDate\"]")).sendKeys("2020-05-20");
		driver.findElement(By.xpath("//*[@id=\"applyleave_txtToDate\"]")).clear();
		driver.findElement(By.xpath("//*[@id=\"applyleave_txtToDate\"]")).sendKeys("2020-05-20");
		driver.findElement(By.xpath("//*[@id=\"applyleave_txtComment\"]")).sendKeys("Testing Leaves");
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id= 'applyBtn']")));
		driver.findElement(By.xpath("//*[@id = 'applyBtn']")).click();
		driver.findElement(By.xpath("//*[@id=\"menu_leave_viewMyLeaveList\"]")).click();
		//wait();
		WebElement from = driver.findElement(By.xpath("//*[@id=\"calFromDate\"]"));
		from.clear();
		from.sendKeys("2020-05-20");
		
		driver.findElement(By.xpath("//*[@id=\"calToDate\"]")).clear();
		driver.findElement(By.xpath("//*[@id=\"calToDate\"]")).sendKeys("2020-05-20");
		driver.findElement(By.xpath("//*[@id=\"btnSearch\"]")).click();
		WebElement verify = driver.findElement(By.linkText("2020-05-20"));
		Assert.assertEquals(verify.getText(), "2020-05-20");
		WebElement status = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/div/form/div[3]/table/tbody/tr/td[6]"));
		System.out.println("Leave status is : " + status.getText());
		System.out.println("Comment : " + driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/div/form/div[3]/table/tbody/tr[1]/td[7]/span")).getText());
	}

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.get("http://alchemy.hguy.co/orangehrm");
		
	}

	@AfterClass
	public void afterClass() {
		driver.close();
	}

}
